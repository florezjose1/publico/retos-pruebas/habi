import json
import pytest
import requests
import requests_mock


@pytest.fixture
def response_success():
    return [
        {
            "id": "71",
            "address": "Malabar entrada 2v",
            "city": "pereira",
            "status": "pre_venta",
            "price": "350000000",
            "description": "Casa campestre con hermosos paisajes",
            "year": "2021"
        },
        {
            "id": "74",
            "address": "Entrada 5 via cerritos",
            "city": "pereira",
            "status": "pre_venta",
            "price": "270000000",
            "description": "Casa campestre con lago",
            "year": "2021"
        },
        {
            "id": "82",
            "address": "Entrada 9 via cerritos",
            "city": "pereira",
            "status": "pre_venta",
            "price": "270000000",
            "description": "Casa campestre con lago",
            "year": "2021"
        },
        {
            "id": "20",
            "address": "Entrada 2 via cerritos",
            "city": "pereira",
            "status": "pre_venta",
            "price": "270000000",
            "description": "Casa campestre con lago",
            "year": "2021"
        }
    ]


@pytest.fixture
def error_filter():
    return [{"message": "FT001: Invalid filter: status_error."}]


@pytest.fixture
def error_status():
    return [{"message": "ST001: Invalid status: comprando."}]


@pytest.fixture
def one_record():
    return {
        "id": "56",
        "address": "calle 23 #45-67r",
        "city": "medellin",
        "status": "pre_venta",
        "price": "210000000",
        "description": "",
        "year": "2002"
    }


@pytest.fixture
def url_app():
    """Return url app base"""
    return 'http://localhost:8080'


def test_success_filter(url_app, response_success):
    url_app += "/api/houses/all?city=bogota&city=pereira&year=2021&status=pre_venta"
    with requests_mock.Mocker() as mock_request:
        mock_request.get(url_app, text=f"{response_success}")
        response = requests.get(url_app)

    assert json.loads(response.text.replace("'", '"')) == response_success


def test_error_filter(url_app, error_filter):
    url_app += "/api/houses/all?status_error=pre_venta"
    with requests_mock.Mocker() as mock_request:
        mock_request.get(url_app, text=f"{error_filter}")
        response = requests.get(url_app)

    assert json.loads(response.text.replace("'", '"')) == error_filter


def test_error_status(url_app, error_status):
    url_app += "/api/houses/all?status=comprando"
    with requests_mock.Mocker() as mock_request:
        mock_request.get(url_app, text=f"{error_status}")
        response = requests.get(url_app)

    assert json.loads(response.text.replace("'", '"')) == error_status


def test_one_record(url_app, one_record):
    url_app += "/api/house/56"
    with requests_mock.Mocker() as mock_request:
        mock_request.get(url_app, text=f"{one_record}")
        response = requests.get(url_app)

    assert json.loads(response.text.replace("'", '"')) == one_record
