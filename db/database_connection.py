"""
This file only connect to DB
Created at: 5-07-2022
"""
from mysql import connector

from settings.settings import DATABASE


class Connection:
    def __init__(self):
        self.host = DATABASE.get('HOST')
        self.port = DATABASE.get('PORT')
        self.user = DATABASE.get('USER')
        self.password = DATABASE.get('PASSWORD')
        self.database = DATABASE.get('NAME')
        self.connection = None

    def __create_connection(self):
        """Return a connection object to the db"""
        return connector.connect(
            host=self.host,
            port=self.port,
            user=self.user,
            password=self.password,
            database=self.database
        )

    def __get_connection(self):
        """Create the connection with the db"""
        self.connection = self.__create_connection()
        return self.connection.cursor()

    @staticmethod
    def __close_connection(cursor, connection):
        """Close the connection with the db"""
        cursor.close()
        connection.close()

    def execute_query(self, query_app: str):
        """Executes query corresponding to the user's wish and
        returns a list with a base structure according to the requirements"""
        cursor = self.__get_connection()
        cursor.execute(query_app)
        data_query = list()
        for row in cursor:
            data_query.append({
                'id': row[0],
                'address': row[1],
                'city': row[2],
                'status': row[3],
                'price': row[4],
                'description': row[5],
                'year': row[6],
            })
        self.__close_connection(cursor, self.connection)
        return data_query
