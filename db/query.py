from db.database_connection import Connection
from immovables.errors import InvalidStatusError, InvalidFilterError


class Query:

    @staticmethod
    def __status_allowed():
        return ['pre_venta', 'en_venta', 'vendido']

    @staticmethod
    def __get_allowed_filters():
        """The keys contain each of the possible filter options,
        and their value contains the pointer to the desired table."""
        return {'city': 'p.city', 'status': 'st.name', 'year': 'p.year', 'id': 'p.id'}

    @classmethod
    def __resolve_filters(cls, **kwargs):
        """Dynamically resolve possible filter options to append to the base query"""
        keys = kwargs.keys()
        _query = ''
        allowed_filters = cls.__get_allowed_filters()
        for k in keys:
            if not k in allowed_filters.keys():
                raise InvalidFilterError(k)

            query_extra = ''
            for value in kwargs.get(k):
                # if it's not an allowed state, we just ignore it
                if k == 'status' and not value in cls.__status_allowed():
                    raise InvalidStatusError(value)

                query_extra += f"'{value}',"

            if k == 'id':
                _query += f" and {allowed_filters.get(k)} = {query_extra[:-1]}"
            else:
                _query += f" and {allowed_filters.get(k)} in ({query_extra[:-1]})"

        return _query

    @classmethod
    def resolve(cls, **kwargs):
        """Bring all the records corresponding to the base query or with the filters passed in the kwargs"""
        try:
            query = """select p.id, p.address, p.city, st.name, p.price, p.description, p.year
                    from habi_db.property as p
                    INNER JOIN (
                    SELECT h.property_id, h.status_id, MAX(h.update_date) AS update_date
                    FROM habi_db.status_history as h
                    GROUP BY h.property_id) p2
                    ON (p.id = p2.property_id)
                    INNER JOIN habi_db.status as st on st.id = p2.status_id
                    WHERE st.id in (3, 4, 5)
                    """

            if kwargs:
                query += cls.__resolve_filters(**kwargs)

            con = Connection()
            data = con.execute_query(query)
        except InvalidStatusError as e:
            data = [{'message': str(e)}]
        except InvalidFilterError as e:
            data = [{'message': str(e)}]
        except Exception as e:
            data = [{'message': f"We have an unexpected error, error: {e}"}]

        return data
