## Habi

#### Parte 1: API (Servicio de consulta)

Para el desarrollo de la prueba donde en lo posible no 
utilizar un framework y/o herramienta de consulta(ORM),
se planeó desarrollarse solo con python y algunas librerías
que nos permiten o nos ayudan a construir el servicio web.

Dentro de estas utilidades utilizadas, tenemos a: `http.server` 
que nos ayuda a exponer el servidor... y a partir de allí, solo librerías como
`mysql` y `pytest` donde para el primero, nos permite conectar a la DB, y el 
segundo para la construcción de las pruebas unitarias.


###### Version python: `3.8`

#### Clone proyecto
```
git clone 
```

#### Configuraciones de proyecto
```
$ cd habi
$ virtualenv -p python3 venv
$ source venv/bin/activate
```

#### Instalación de requerimientos
```
pip3 install -r requirements.txt
```

#### Configuraciones .env 

Crea un archivo .env con los siguientes valores

```python
ALLOWED_HOSTS=localhost

HOST_DB=
PORT_DB=
USER_DB=
PASSWORD_DB=
NAME_DB=

```

#### Correr proyecto 
```
python3 manage.py 
```


#### Servicio de consulta: Ejemplo
##### Listar las propiedades

* #### Todas (GET) `/api/properties/all`

```javascript
fetch('http://localhost:8080/api/properties/all', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Filtros (GET) `/api/properties/all?options`

#### `options: city, year, status`

`status: pre_venta, en_venta, vendido`

Ejemplo de un solo filtro:

```javascript
fetch('http://localhost:8080/api/properties/all?city=bogota', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

Ejemplo con más filtros:

```javascript
fetch('http://localhost:8080/api/properties/all?city=bogota&year=2020&status=pre_venta', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

Ejemplo de filtro para varias ciudades o `options`:

```javascript
fetch('http://localhost:8080/api/properties/all?city=bogota&city=pereira', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Obtener un registro por ID (GET) `/api/property/ID`

```javascript
fetch('http://localhost:8080/api/property/55', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```


-------------------------------------------------------

#### Part 2: Servicio de "Me gusta"

Para la parte 2 del ejercicio, se deja la imagen correspondiente al diagrama
y a su vez, el sql para crear dicha estructura.
Dicha información se encuentra en la carpeta de: `service_like`



Made with ♥ by [Jose Florez](www.joseflorez.co)
