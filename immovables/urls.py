import re

from immovables.views import *
from servers.status import *
from settings.response_messages import *

home = HomeView()
filter_records = FilterRecords()
one = OneRecord()


class URL:
    @staticmethod
    def __resolve_path(path):
        """Dynamically resolves the user's path, returning its proper address"""
        paths = [
            ('/', home),
            ('/api/properties/all', filter_records),
        ]

        for p in paths:
            if path == p[0]:
                return True, p[1]

        return False, None

    @staticmethod
    def __resolve_filters(path):
        """Resolves possible filters submitted by the user"""
        filters = path.split('&')
        data_filters = dict()
        for param in filters:
            params = param.split('=')
            if not params[0] in data_filters:
                data_filters[params[0]] = list()
            data_filters[params[0]].append(params[1])

        return data_filters

    @classmethod
    def get_path(cls, path, request_type):
        """URL controller, resolves according to the path, the corresponding redirection"""
        if request_type == 'GET':
            # Searched if there is any match with the path to get a single record
            path_api = re.findall("/api/property/\d+", path)
            if path_api:
                # We get the id to look for
                _pk = re.findall("\d+", path)
                paths = [
                    ('/api/property/{}'.format(_pk[0]), one.get(pk=_pk[0])),
                ]

                if any(path in url for url in paths):
                    if path == '/api/property/{}'.format(_pk[0]):
                        return paths[0][1].status_code, paths[0][1].data
                return HTTP_404_NOT_FOUND, URL_NOT_FOUND

            re_path = path.split('?')
            is_path, view = cls.__resolve_path(re_path[0])
            if is_path:
                extra_data = cls.__resolve_filters(re_path[1]) if len(re_path) > 1 else {}
                response = view.get(**extra_data)
                return response.status_code, response.data
            else:
                return HTTP_404_NOT_FOUND, URL_NOT_FOUND
