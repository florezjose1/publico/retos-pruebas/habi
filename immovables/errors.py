class Error(Exception):
    pass


class InvalidStatusError(Error):
    error_code = "ST001"

    def __init__(self, status):
        self.status = status

    def __str__(self):
        return f"{self.error_code}: Invalid status: {self.status}."


class InvalidFilterError(Error):
    error_code = "FT001"

    def __init__(self, status):
        self.status = status

    def __str__(self):
        return f"{self.error_code}: Invalid filter: {self.status}."
