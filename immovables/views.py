from db.query import Query
from servers.response import Response
from servers import status


class HomeView:
    def get(self, **kwargs):
        data = 'Welcome to Habi API'
        return Response(data=data, status=status.HTTP_200_OK)


class FilterRecords:
    def get(self, **kwargs):
        data = Query.resolve(**kwargs)
        return Response(data=data, status=status.HTTP_200_OK)


class OneRecord:
    def get(self, pk=None, **kwargs):
        return_data = Query.resolve(**{'id': [pk]})
        print(return_data)
        if return_data:
            return Response(data=return_data[0], status=status.HTTP_200_OK)

        return Response(data={'message': 'Record no found'}, status=status.HTTP_404_NOT_FOUND)
