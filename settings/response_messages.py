"""
All response messages
created_date: 05-07-2022
"""
__author__ = 'Jose Florez'
__version__ = '1.0.0'

# common messages
URL_NOT_FOUND = {"message": "URL not found"}
DATA_NOT_FOUND = {"message": "Data not found"}
INVALID_REQUEST = {"message": "Invalid request"}

