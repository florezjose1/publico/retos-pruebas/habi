"""
All settings information of this Rest API project will hold here
Created at: 05-07-2022
"""
__author__ = "Jose Florez"

import os

import environ

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Read environment variables
env = environ.Env(
    ALLOWED_HOSTS=list,
)
envfile_path = os.path.join(BASE_DIR, '.env')
environ.Env.read_env(envfile_path)

# ALLow hosts
ALLOWED_HOSTS = env('ALLOWED_HOSTS')

# Database settings
DATABASE = {
    'HOST': env('HOST_DB'),
    'PORT': env('PORT_DB'),
    'USER': env('USER_DB'),
    'PASSWORD': env('PASSWORD_DB'),
    'NAME': env('NAME_DB'),
}
