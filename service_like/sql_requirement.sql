-- Syntax for MySQL Database
CREATE TABLE user (
    id INT(9) NOT NULL PRIMARY KEY,
    username VARCHAR(20) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE property (
    id INT(9) NOT NULL PRIMARY KEY,
    address VARCHAR(150),
    city VARCHAR(100),
    price NUMBER(10, 2),
    year INT(4),
    description TEXT
);

CREATE TABLE like (
    id INT(9) NOT NULL PRIMARY KEY,
    user_id INT(9) NOT NULL,
    property_id INT(9) NOT NULL,
    date_like DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (property_id) REFERENCES property(id)
);